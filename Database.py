#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  6 10:01:41 2021

@author: victor
"""
import os.path
import pandas as pd

class Database:
    
    def __init__(self):
        self._covid_df = pd.DataFrame()
        self._covid_latest_date = None
        self._life_expectancy_df = pd.DataFrame()
        self._life_expectancy_latest_year = None
        self._obese_data = pd.DataFrame()
        self._obese_data_latest_year = None
        self._urbanization_data = pd.DataFrame()        
        self._urbanization_data_latest_year = None
        self.setup()


    def setup(self):
        self.set_covid_data()
        self.set_life_expectancy_data()
        self.set_obese_data()
        self.set_urbanization_data()
        
        
        
    def set_covid_data(self):
        fname = 'data/owid-covid-data.csv'
        if os.path.isfile(fname):
            print(f'Reading data from local file {fname}. If you prefer to download last version, remove it.')
            df = pd.read_csv(fname)
        else:
            url = 'https://covid.ourworldindata.org/data/owid-covid-data.csv'
            print(f'Downloading world covid data from {url}.')
            df = pd.read_csv(url)
            df.to_csv(fname)
            print(f'Fifle saved at {fname}.')   
        self._covid_df = df
        self._covid_latest_date = max(df.date) 
        
    def get_covid_latest_date(self):
        df = self._covid_df
        df = df.loc[df.date==self._covid_latest_date]
        return df
        
    def get_covid_selected_data(self):
        selected_columns = ['iso_code','continent','location','total_cases_per_million','total_deaths_per_million']
        df = self.get_covid_latest_date()
        df = df[selected_columns]
        df['mortality'] = df['total_deaths_per_million'] / df['total_cases_per_million']
        return df.set_index('iso_code',drop=True)
    
#################################################################################################################    

    def set_life_expectancy_data(self):
        fname = 'data/life-expectancy.csv'
        if os.path.isfile(fname):
            print(f'Reading data from local file {fname}. If you prefer to download last version, remove it.')
            df = pd.read_csv(fname)
        else:
            url = 'https://ourworldindata.org/life-expectancy'
            print(f'Please download file from here {url}.')
            df = pd.read_csv(url)
            df.to_csv(fname)
            print(f'Fifle saved at {fname}.')
        self._life_expectancy_df = df
        
        self._life_expectancy_latest_year = max(df.Year)
        
    def get_life_expectancy_data(self):
        return self._life_expectancy_df
    
    def get_life_expectancy_latest_year(self):
        df = self._life_expectancy_df
        df = df.loc[df.Year==self._life_expectancy_latest_year].set_index('Code',drop=True)
        return df.iloc[:,-1]

#################################################################################################################

    def set_obese_data(self):
        fname = 'data/share-of-adults-defined-as-obese.csv'
        if os.path.isfile(fname):
            print(f'Reading data from local file {fname}. If you prefer to download last version, remove it.')
            df = pd.read_csv(fname)
        else:
            url = 'https://ourworldindata.org/obesity'
            print(f'Please download file from here {url}.')
            df = pd.read_csv(url)
            df.to_csv(fname)
            print(f'Fifle saved at {fname}.')
        self._obese_data = df
        
        self._obese_data_latest_year = max(df.Year)
        
    def get_obese_data(self):
        return self._obese_data
    
    def get_obese_data_latest_year(self):
        df = self._obese_data
        df = df.loc[df.Year==self._obese_data_latest_year].set_index('Code',drop=True)
        obesity_series = df.iloc[:,-1]
        obesity_series.name = 'Obesity'
        return obesity_series

#################################################################################################################

    def set_urbanization_data(self):
        fname = 'data/share-of-population-urban.csv'
        if os.path.isfile(fname):
            print(f'Reading data from local file {fname}. If you prefer to download last version, remove it.')
            df = pd.read_csv(fname)
        else:
            url = 'https://ourworldindata.org/urbanization'
            print(f'Please download file from here {url}.')
            df = pd.read_csv(url)
            df.to_csv(fname)
            print(f'Fifle saved at {fname}.')
        self._urbanization_data = df
        
        self._urbanization_data_latest_year = max(df.Year)
        
    def get_urbanization_data(self):
        return self._urbanization_data
    
    def get_urbanization_data_latest_year(self):
        df = self._urbanization_data
        df = df.loc[df.Year==self._urbanization_data_latest_year].set_index('Code',drop=True)
        urbanization_series = df.iloc[:,-1]
#         obesity_series.name = 'Urbanization'
        return urbanization_series

#################################################################################################################

database = Database()



# get_last_date

# dates = df.date.unique()
# dates.sort()
# last_date = dates[-1]
# print(last_date)
# df = df.loc[df.date==last_date]

# # def main():
# df_complete = get_world_data()
# df = df_complete.copy()
# dates = df.date.unique()
# dates.sort()
# last_date = dates[-1]
# print(last_date)
# df = df.loc[df.date==last_date]

# selected_columns = ['iso_code','continent','location','total_cases_per_million','total_deaths_per_million','icu_patients_per_million',
#                     'total_tests_per_thousand']

# df = df[selected_columns]
    
    

# if __name__ == "__main__":
#     main()